<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

use App\Services\EntityService;

/**
 * Class EntityServiceTest
 * Test class for \Services\EntityService methods.
 * @package App\Test
 */
class EntityServiceTest extends TestCase
{
    /**
     * Test if the constructor's dependencies are being injected correctly.
     */
    public function testInstanceConstruction()
    {
        $obj = app(EntityService::class);
        $this->assertInstanceOf(EntityService::class, $obj);
    }

    /**
     * Test if valid query passes the validation.
     */
    public function testValidateRequestDataValidQuery()
    {

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'validateRequestData');

        $data = [
            'query' => 'Erdinger',
            'type' => 'beer'
        ];

        $response = $method->invokeArgs($obj, [$data]);

        $this->assertTrue($response);
    }

    /**
     * Test if invalid query generates exception when validated.
     */
    public function testValidateRequestDataInvalidQuery()
    {

        $this->expectException(\Illuminate\Validation\ValidationException::class);

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'validateRequestData');

        $data = [
            'query' => 'Erding&r',
            'type' => 'beer'
        ];

        $response = $method->invokeArgs($obj, [$data]);
        $this->assertTrue($response);
    }

    /**
     * Test if valid type passes the validation.
     */
    public function testValidateRequestDataValidType()
    {

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'validateRequestData');

        $data = [
            'query' => 'Erdinger',
            'type' => 'beer'
        ];

        $response = $method->invokeArgs($obj, [$data]);
        $this->assertTrue($response);
    }

    /**
     * Test if invalid type generates exception when validated.
     */
    public function testValidateRequestDataInvalidType()
    {

        $this->expectException(\Illuminate\Validation\ValidationException::class);

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'validateRequestData');

        $data = [
            'query' => 'Erdinger',
            'type' => 'b33r'
        ];

        $method->invokeArgs($obj, [$data]);
    }

    /**
     * Test if invalid search type generates exception.
     */
    public function testSearchInvalidType()
    {

        $this->expectException(\Exception::class);

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'search');

        $query = 'Erdinger';
        $type = 'b33r';

        $method->invokeArgs($obj, [$query, $type]);
    }

    /**
     * Test if search resturns expected result.
     */
    public function testSearchValidType()
    {

        $obj = app(EntityService::class);
        $method = self::reflectMethod(EntityService::class, 'search');

        $query = 'Erdinger';
        $type = 'beer';

        $response = $method->invokeArgs($obj, [$query, $type]);

        $this->assertInternalType("array", $response);
        $this->assertGreaterThan(0, count($response));
    }
}
