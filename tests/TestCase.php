<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

/**
 * Class TestCase
 * @package App\Test
 */
class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * Creates the application.
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        return $app;
    }

    /**
     * Creates method reflection.
     *
     * @param string $class
     * @param string $method
     * @return ReflectionMethod
     */
    public static function reflectMethod($class, $method)
    {
        $reflection = new ReflectionClass($class);

        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }
}
