<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

use App\Services\ApiService;

/**
 * Class ApiServiceTest
 * Test class for \Services\ApiService methods.
 * @package App\Test
 */
class ApiServiceTest extends TestCase
{
    /**
     * Test if the constructor's dependencies are being injected correctly.
     */
    public function testInstanceConstruction()
    {
        $obj = app(ApiService::class);
        $this->assertInstanceOf(ApiService::class, $obj);
    }

    /**
     * Test if request returns expected result
     */
    public function testRequestValid()
    {
        $obj = app(ApiService::class);
        $method = self::reflectMethod(ApiService::class, 'request');

        $path = '/beers';

        $params = [
            'ibu' => '-900',
            'order' => 'random',
            'randomCount' => 1
        ];

        $response = $method->invokeArgs($obj, [$path, $params]);

        $this->assertInstanceOf(stdClass::class, $response);
    }


    /**
     * Test if request fails when invalid path is provided.
     */
    public function testRequestInvalidPath()
    {
        $this->expectException(\Exception::class);

        $obj = app(ApiService::class);
        $method = self::reflectMethod(ApiService::class, 'request');

        $path = null;

        $method->invokeArgs($obj, [$path]);
    }
}
