<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

use App\Services\BreweryService;

/**
 * Class BreweryServiceTest
 * Test class for \Services\BreweryService methods.
 * @package App\Test
 */
class BreweryServiceTest extends TestCase
{
    /**
     * Test if the constructor's dependencies are being injected correctly.
     */
    public function testInstanceConstruction()
    {
        $obj = app(BreweryService::class);
        $this->assertInstanceOf(BreweryService::class, $obj);
    }

    /**
     * Test if get beers by brewery returns valid results.
     */
    public function testGetBeersByBreweryResponseValid()
    {

        $obj = app(BreweryService::class);
        $method = self::reflectMethod(BreweryService::class, 'getBeersByBrewery');

        $brewery_id = 'q6vJUK';
        $response = $method->invokeArgs($obj, [$brewery_id]);

        $this->assertInternalType("array", $response);
        $this->assertGreaterThan(0, count($response));
        $this->assertArrayHasKey('id', $response[0]);
    }

    /**
     * Test if get beers by brewery returns invalid results.
     */
    public function testGetBeersByBreweryResponseInvalid()
    {

        $this->expectException(\Exception::class);

        $obj = app(BreweryService::class);
        $method = self::reflectMethod(BreweryService::class, 'getBeersByBrewery');

        $brewery_id = 'qwerty';
        $method->invokeArgs($obj, [$brewery_id]);
    }
}
