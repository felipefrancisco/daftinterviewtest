<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

use App\Services\BeerService;

/**
 * Class BeerServiceTest
 * Test class for \Services\BeerService methods.
 * @package App\Test
 */
class BeerServiceTest extends TestCase
{
    /**
     * Test if the constructor's dependencies are being injected correctly.
     */
    public function testInstanceConstruction()
    {
        $obj = app(BeerService::class);
        $this->assertInstanceOf(BeerService::class, $obj);
    }

    /**
     * Test if filter beers returns expected result.
     */
    public function testFilterBeersWithResults()
    {

        $obj = app(BeerService::class);
        $method = self::reflectMethod(BeerService::class, 'filterBeers');

        $item = new stdClass();

        $item->labels = [];
        $item->description = 'Lorem Ipsum';
        $item->breweries = [
            [
                'id' => 'q6vJUK'
            ]
        ];

        $data = [
            $item
        ];

        $response = $method->invokeArgs($obj, [$data]);

        $this->assertInternalType("array", $response);
        $this->assertGreaterThan(0, count($response));
    }

    /**
     * Test if filter beers returns empty array when item has
     * empty "breweries" key.
     */
    public function testFilterBeersBreweriesRuleNoResult()
    {

        $obj = app(BeerService::class);
        $method = self::reflectMethod(BeerService::class, 'filterBeers');

        $item = new stdClass();

        $item->labels = [];
        $item->description = 'Lorem Ipsum';
        $item->breweries = [];

        $data = [
            $item
        ];

        $response = $method->invokeArgs($obj, [$data]);

        $this->assertInternalType("array", $response);
        $this->assertEquals(0, count($response));
    }

    /**
     * Test if filter beers returns empty array when item has
     * no "description" key.
     */
    public function testFilterBeersDescriptionRuleNoResult()
    {

        $obj = app(BeerService::class);
        $method = self::reflectMethod(BeerService::class, 'filterBeers');

        $item = new stdClass();

        $item->labels = [];

        $data = [
            $item
        ];

        $response = $method->invokeArgs($obj, [$data]);

        $this->assertInternalType("array", $response);
        $this->assertEquals(0, count($response));
    }

    /**
     * Test if filter beers returns empty array when item has
     * no "labels" key.
     */
    public function testFilterBeersLabelsRuleNoResult()
    {

        $obj = app(BeerService::class);
        $method = self::reflectMethod(BeerService::class, 'formatBeerResponse');

        $item = new stdClass();

        $item->id = 'lorem';
        $item->name = 'lorem';
        $item->description = 'lorem';

        $item->labels = new stdClass();
        $item->labels->medium = 'lorem';

        $response = $method->invokeArgs($obj, [$item]);

        $this->assertInternalType("array", $response);
        $this->assertEquals([
            'id' => 'lorem',
            'name' => 'lorem',
            'description' => 'lorem',
            'picture' => 'lorem'
        ], $response);
    }

    /**
     * Test of get random beers returns expected result.
     */
    public function testGetRandomBeers()
    {

        $obj = app(BeerService::class);
        $method = self::reflectMethod(BeerService::class, 'getRandomBeers');
        
        $response = $method->invokeArgs($obj, []);

        $this->assertInternalType("array", $response);
        $this->assertGreaterThan(0, count($response));
        $this->assertArrayHasKey('id', $response[0]);
    }
}
