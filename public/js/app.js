/**
 * Application Config
 * @type {{swal: (*|sweetAlert), basil: *, angular: angular.Module}}
 */
var app = {
    "swal": swal,
    "basil": new window.Basil(),
    "angular": angular.module('breweryApiTest', [])
}

/**
 * Message Service
 */
app.angular.service('MessageService', [function () {

    /**
     * Display error message.
     * @param data
     */
    this.error = function(data) {

        app.swal({
            title: 'Error!',
            text: data.message,
            type: 'error',
            confirmButtonText: 'OK'
        })
    }

    return this;

}]);

/**
 * BreweryService
 */
app.angular.service('BreweryService', ['$http', 'MessageService', function ($http, MessageService) {

    /**
     * Request random beers.
     * @returns {*}
     */
    this.getRandomBeers = function() {

        return $http.get('/api/beers/random')
            .error(function(data) {

                MessageService.error(data);
            });
    }

    /**
     * Request beers from brewery.
     * @param brewery_id
     * @returns {*}
     */
    this.getBeersByBrewery = function(brewery_id) {

        var params = {};

        if (brewery_id) {
            params.brewery_id = brewery_id;
        }

        return $http.get('/api/breweries/beers', {
            'params': params
        })
        .error(function(data) {

            MessageService.error(data);
        });
    }

    return this;
}]);

/**
 * EntityService
 */
app.angular.service('EntityService', ['$http', 'MessageService', function ($http, MessageService) {

    this.search = function(query, type) {

        var params = {
            "query": query,
            "type": type
        };

        return $http.get('/api/entities/search', {
            'params': params
        })
        .error(function(data) {

            MessageService.error(data);
        });
    }


    return this;

}]);

/**
 * HomeController
 */
app.angular.controller('HomeController', ['$scope', '$timeout', 'BreweryService', 'EntityService',  function ($scope, $timeout, BreweryService, EntityService) {

    /**
     * Self instance
     */
    var vm = this;

    /**
     * Default beer object.
     * @type {{id: null, brewery: null, name: null, description: null, picture: null}}
     */
    var beer = {
        "id": null,
        "brewery": null,
        "name": null,
        "description": null,
        "picture": null
    };

    /**
     * Default search object.
     * @type {{results: Array, query: string, type: string}}
     */
    var srch = {
        "results": [],
        "query": "",
        "type": 'beer'
    };

    this.searched = false;
    $scope.quantity = 5;

    $scope.srch = srch;
    $scope.beer = beer;

    /**
     * Events property
     * @type {{}}
     */
    this.events = {};

    /**
     * Perform search of entities using "query" and "type".
     */
    this.events.performSearch = function() {

        var promise = EntityService.search($scope.srch.query, $scope.srch.type);

        promise.success(function(data) {

            vm.setResults(data);
        });
    }

    /**
     * Check if there's any beers on the localStorage.
     * If the localStorage is empty, request new random beers.
     */
    this.events.getRandomBeers = function() {

        var data = app.basil.get('random.beers');

        if(data && data.length > 0) {

            vm.setRandomBeer(data);
            return;
        }

        var promise = BreweryService.getRandomBeers();

        promise.success(function(data) {

            vm.setRandomBeer(data);
        });
    }

    /**
     * Get beers from brewery.
     */
    this.events.getBeersByBrewery = function() {

        var brewery = $scope.beer.brewery;

        var promise = BreweryService.getBeersByBrewery(brewery);

        promise.success(function(data) {

            vm.setResults(data);
        });
    }

    /**
     * Store the random beers received form the server in the local storage.
     * @param data
     */
    this.setRandomBeer = function(data) {

        vm.setBeer(data.pop());
        app.basil.set('random.beers', data);
    }

    /**
     * Set the displayed beer.
     * @param data
     */
    this.setBeer = function(data) {

        $scope.beer = data;
    }

    /**
     * Set the displayed results.
     * @param data
     */
    this.setResults = function(data) {

        this.searched = true;
        $scope.max = 5;
        $scope.srch.results = data;
    }

    /**
     * Only display search results if the "Search"
     * button was clicked once.
     * @returns {boolean}
     */
    this.displaySearchResults = function () {

        return this.searched;
    }

    /**
     * Disable search button when search field
     * is empty.
     * @returns {boolean}
     */
    this.disableSearchButton = function() {

        return $scope.srch.query.length == 0;
    }

    /**
     * Display "show more" if there's still
     * results not being displayed on screen.
     * @returns {boolean}
     */
    this.displayShowMore = function() {

        return $scope.quantity < $scope.srch.results.length;
    }

    /**
     * Raise the number of results being displayed
     * by 5.
     */
    this.showMore = function() {

        $scope.quantity += 5;
    }

    /**
     * Get random beers as soon as the user loads the page.
     */
    this.events.getRandomBeers();

    return vm;

}]);