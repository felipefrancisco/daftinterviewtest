<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use App\Services\BeerService;
use Illuminate\Http\Request;

/**
 * Class BeerController
 * @package App\Http\Controllers
 */
class BeerController extends Controller
{
    /**
     * BeerService instance.
     * @var BeerService
     */
    protected $beerService;

    /**
     * Request instance.
     * @var Request
     */
    protected $request;

    /**
     * BeerController constructor.
     * @param BeerService $beerService
     * @param Request $request
     */
    public function __construct(BeerService $beerService, Request $request)
    {
        $this->beerService = $beerService;
        $this->request = $request;
    }

    /**
     * Random beers endpoint.
     * @return mixed
     * @throws \Exception
     */
    public function random()
    {
        $response = $this->beerService->getRandomBeers();

        return response()->json($response);
    }
}
