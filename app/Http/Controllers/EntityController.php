<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use App\Services\EntityService;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

/**
 * Class EntityController
 * @package App\Http\Controllers
 */
class EntityController extends Controller
{
    /**
     * EntityService instance.
     * @var EntityService
     */
    protected $entityService;

    /**
     * Request instance.
     * @var Request
     */
    protected $request;

    /**
     * EntityController constructor.
     * @param EntityService $entityService
     * @param Request $request
     */
    public function __construct(EntityService $entityService, Request $request)
    {
        $this->entityService = $entityService;
        $this->request = $request;
    }

    /**
     * Entity Search endpoint.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function search()
    {
        try {
            $data = [
                'type'  => $this->request->get('type'),
                'query' => $this->request->get('query')
            ];

            $this->entityService->validateRequestData($data);

            $query = $this->request->get('query');
            $type = $this->request->get('type');

            $response = $this->entityService->search($query, $type);

            return response()->json($response);
        } catch (ValidationException $e) {
            return $this->error($e);
        }
    }
}
