<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use App\Services\BreweryService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class BreweryController
 * @package App\Http\Controllers
 */
class BreweryController extends Controller
{
    /**
     * BreweryService instance
     * @var BreweryService
     */
    protected $breweryService;

    /**
     * Request instance
     * @var Request
     */
    protected $request;

    /**
     * BreweryController constructor.
     * @param BreweryService $breweryService
     * @param Request $request
     */
    public function __construct(BreweryService $breweryService, Request $request)
    {
        $this->breweryService = $breweryService;
        $this->request = $request;
    }

    /**
     * Index
     * @return mixed
     */
    public function home()
    {
        return view('brewery.home');
    }

    /**
     * Brewery's Beers endpoint
     * @return mixed
     * @throws \Exception
     */
    public function beers()
    {
        try {
            $this->validate($this->request, [
                'brewery_id' => 'required|string'
            ]);

            $brewery_id = $this->request->get('brewery_id');

            $response = $this->breweryService->getBeersByBrewery($brewery_id);

            return response()->json($response);
        } catch (ValidationException $e) {
            return $this->error($e);
        }
    }
}
