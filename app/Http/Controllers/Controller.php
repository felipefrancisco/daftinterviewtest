<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\ValidationException;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Returns formatted error response.
     * @param \Exception $e
     * @return JsonResponse
     */
    public function error(\Exception $e)
    {
        $message = $e->getMessage();

        if ($e instanceof ValidationException) {
            $messageBag = $e->validator->getMessageBag()->getMessages();

            $message = [];
            foreach ($messageBag as $messages) {
                foreach ($messages as $m) {
                    $message[] = $m;
                }
            }

            $message = implode("\n", $message);
        }

        return response()->json([
            'message' => $message
        ], 422);
    }
}
