<?php
/**
 * Memcached Client class
 */

namespace App\Library\Memcached;

/**
 * Class Client
 * @package App\Library\Memcached
 */
class Client
{
    /**
     * Memcached instance.
     * @var Mmecached
     */
    public static $instance;

    /**
     * Memcached host.
     * @var string
     */
    protected $host;

    /**
     * Memcached port.
     * @var int
     */
    protected $port;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->host = env('MEMCACHED_HOST');
        $this->port = env('MEMCACHED_PORT');

        $this->instance();
    }

    /**
     * Returns instance.
     * @return mixed
     * @throws \Exception
     */
    public function instance()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }

        if (extension_loaded('memcached')) {
            self::$instance = new \Memcached();
            $this->instance()->addServer($this->host, $this->port);
        } else {
            throw new \Exception('Memcached not found');
        }
    }

    /**
     * Set an entry on Memecache
     * @param string $key
     * @param mixed $data
     * @param int $ttl
     * @return $this
     * @throws \Exception
     */
    public function set($key, $data, $ttl)
    {
        $class = get_class($this->instance());

        if ($class == 'Memcached') {
            $this->instance()->set($key, $data, $ttl);
        } elseif ($class == 'Memcache') {
            $this->instance()->set($key, $data, false, $ttl);
        }

        return $this;
    }

    /**
     * Get an entry from Memcached
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function get($key)
    {
        return $this->instance()->get($key);
    }

    /**
     * Remove an entry from Memcached
     * @param string $key
     * @return $this
     * @throws \Exception
     */
    public function forget($key)
    {
        $this->instance()->set($key, null);
        return $this;
    }
}
