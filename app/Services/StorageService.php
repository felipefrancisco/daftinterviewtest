<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Services;

use App\Library\Memcached\Client;

/**
 * Class StorageService
 * @package App\Services
 */
class StorageService extends Service
{
    /**
     * @var Client
     */
    protected $instance;

    /**
     * @var bool
     */
    protected $isAvailable;

    /**
     * StorageService constructor.
     */
    public function __construct()
    {
        try {
            $this->instance = new Client();
            $this->isAvailable = true;
        } catch (\Exception $e) {
            $this->isAvailable = false;
        }
    }
    /**
     * Remove an entry from the cache storage.
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->instance->get($key);
    }

    /**
     * Remove an entry from the cache storage.
     * @param $key
     * @param $value
     * @param int $ttl
     * @return $this
     */
    public function set($key, $value, $ttl = 60)
    {
        return $this->instance->set($key, $value, $ttl);
    }

    /**
     * Remove an entry from the cache storage.
     * @param string $key
     * @return mixed
     */
    public function forget($key)
    {
        return $this->instance->forget($key);
    }

    /**
     * Check if the cache storage is available.
     * @return bool
     */
    public function isAvailable()
    {

        return $this->isAvailable;
    }
}
