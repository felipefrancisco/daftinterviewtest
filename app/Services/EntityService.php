<?php

namespace App\Services;

use Validator;

/**
 * Class EntityService
 * @package App\Services
 */
class EntityService extends Service
{
    /**
     * ApiService instance.
     * @var ApiService
     */
    protected $apiService;

    /**
     * EntityService constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * Perform search on entities
     * @param string $query
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function search($query, $type)
    {
        $path = '/search';

        $params = [
            'q' => $query,
            'type' => $type
        ];

        $response = $this->apiService->request($path, $params);

        if (!property_exists($response, 'data')) {
            return [];
        }

        $data = $response->data;

        switch ($type) {
            case ('beer'):
                $data = $this->formatBeerEntities($data);
                break;

            case ('brewery'):
                $data = $this->formatBreweryEntities($data);
                break;

            default:
                throw new \Exception('Invalid search type received.');
        }

        return $data;
    }

    /**
     * Format beers received from search.
     * @param array $data
     * @return array
     */
    protected function formatBeerEntities(array $data)
    {
        $result = [];
        foreach ($data as $item) {
            if (!property_exists($item, 'labels') || !property_exists($item, 'description')) {
                continue;
            }

            $row = [
                'name' => $item->name,
                'description' => $item->description,
                'picture' => $item->labels->medium
            ];

            $result[] = $row;
        }

        return $result;
    }

    /**
     * Format breweries received from search.
     * @param $data
     * @return array
     */
    protected function formatBreweryEntities($data)
    {
        $result = [];
        foreach ($data as &$item) {
            if (!property_exists($item, 'images') || !property_exists($item, 'description')) {
                continue;
            }

            $row = [
                'name' => $item->name,
                'description' => $item->description,
                'picture' => $item->images->medium
            ];

            $result[] = $row;
        }

        return $result;
    }

    /**
     * Validates the query request
     * @param array $data
     * @return bool
     */
    public function validateRequestData(array $data)
    {
        $validator = Validator::make($data, [
            'query' => 'required|max:60|regex:/^[a-z0-9 \-]+$/i',
            'type' => 'required|in:beer,brewery'
        ]);

        $validator->validate();

        return true;
    }
}
