<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 09/25/2016
 * Time: 12:44 AM
 */

namespace App\Services;

use Exception;
use GuzzleHttp\Client;

/**
 * Class ApiService
 * @package App\Services
 */
class ApiService extends Service
{
    /**
     * Http Client Instance
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * API Host
     * @var string
     */
    protected $host;

    /**
     * API Key
     * @var string
     */
    protected $key;

    /**
     * @var StorageService
     */
    protected $storageService;

    /**
     * ApiService constructor.
     * @param Client $client
     * @param StorageService $storageService
     * @throws Exception
     */
    public function __construct(Client $client, StorageService $storageService)
    {
        if (!$host = env('BREWERYDB_API_HOST')) {
            throw new Exception('Invalid API Host, please check .env', 1001);
        }

        if (!$key = env('BREWERYDB_API_KEY')) {
            throw new Exception('Invalid API Key, please check .env', 1002);
        }

        $this->storageService = $storageService;

        $this->client = $client;
        $this->host = $host;
        $this->key = $key;
    }

    /**
     * Creates the request to the API, also formats the URL and adds all
     * mandatory data to the parameters.
     * @param string $path
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function request($path, array $params = [])
    {
        if (!is_string($path)) {
            throw new Exception('Invalid path provided.', 1003);
        }

        $url = $this->host() . $path;

        $params += [
            'key' => $this->key()
        ];

        $params = [
            'query' => $params
        ];

        if ($this->storageService->isAvailable() && $path != '/beers') {
            $cacheKey = md5($url . serialize($params));

            if (!$data = $this->storageService->get($cacheKey)) {
                $response = $this->client()->request('GET', $url, $params);
                $data = $response->getBody()->__toString();

                $this->storageService->set($cacheKey, $data, 3600);
            }
        } else {
            $response = $this->client()->request('GET', $url, $params);
            $data = $response->getBody()->__toString();
        }

        return json_decode($data);
    }

    /**
     * Retrieves the HTTP Client instance.
     * @return \GuzzleHttp\Client
     */
    protected function client()
    {
        return $this->client;
    }

    /**
     * Retrieves the API Host.
     * @return string|null
     */
    protected function host()
    {
        return $this->host;
    }

    /**
     * Retrieves the API Key.
     * @return string|null
     */
    protected function key()
    {
        return $this->key;
    }
}
