<?php

namespace App\Services;

use Exception;

/**
 * Class BeerService
 * @package App\Services
 */
class BeerService extends Service
{
    /**
     * ApiService instance.
     * @var ApiService
     */
    protected $apiService;

    /**
     * BeerService constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * Filter beers from BreweryDB. Only beers with description, label and
     * brewery data are elligible to be displayed.
     * @param array $data
     * @return array
     */
    public function filterBeers(array $data)
    {
        return array_filter($data, function ($item) {

            if (!property_exists($item, 'description') || !property_exists($item, 'labels')) {
                return false;
            }

            if (property_exists($item, 'breweries')) {
                if (count($item->breweries) == 0) {
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * Format the beer array containing Id, Name, Description and Picture.
     * @param \stdClass $item
     * @return array
     */
    public function formatBeerResponse(\stdClass $item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'picture' => $item->labels->medium
        ];
    }

    /**
     * Get random beers from BreweryDB.
     * @return array
     * @throws Exception
     */
    public function getRandomBeers()
    {
        $path = '/beers';

        $params = [
            'hasLabels' => 'Y',
            'withBreweries' => 'Y',
            'randomCount' => 10,
            'ibu' => '-900',
            'order' => 'random'
        ];

        $response = $this->apiService->request($path, $params);

        if ($response->totalResults == 0) {
            throw new Exception('No Beers were found, please, try again.');
        }

        $data = $response->data;
        $data = $this->filterBeers($data);

        if (count($data) == 0) {
            throw new Exception('No Beers from this brewery were found.');
        }

        foreach ($data as $k => &$item) {
            $brewery = $item->breweries[0]->id;

            $item = $this->formatBeerResponse($item);
            $item['brewery'] = $brewery;
        }

        sort($data);

        return $data;
    }
}
