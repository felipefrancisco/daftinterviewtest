<?php

namespace App\Services;

use Exception;

/**
 * Class BreweryService
 * @package App\Services
 */
class BreweryService extends Service
{
    /**
     * ApiService instance.
     * @var ApiService
     */
    protected $apiService;

    /**
     * BeerService instance.
     * @var BeerService
     */
    protected $beerService;

    /**
     * BreweryDbService constructor.
     * @param ApiService $apiService
     * @param BeerService $beerService
     */
    public function __construct(ApiService $apiService, BeerService $beerService)
    {
        $this->apiService = $apiService;
        $this->beerService = $beerService;
    }


    /**
     * Get beers of a brewery from BreweryDB.
     * @param string $brewery_id
     * @return array
     * @throws Exception
     */
    public function getBeersByBrewery($brewery_id)
    {
        $path = sprintf('/brewery/%s/beers', $brewery_id);

        $response = $this->apiService->request($path);

        $data = $response->data;
        $data = $this->beerService->filterBeers($data);

        if (count($data) == 0) {
            throw new Exception('No Beers from this brewery were found.');
        }

        foreach ($data as $k => &$item) {
            $item = $this->beerService->formatBeerResponse($item);
        }

        sort($data);

        return $data;
    }
}
