<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'BreweryController@home');

$options = [
    'prefix' => 'api',
    //'middleware' => 'auth:api'
];

Route::group($options, function () {

    /**
     * @api {get} /api/beers/random Request a Random Beer
     * @apiName RandomBeer
     * @apiGroup Beer
     */
    Route::get('/beers/random', 'BeerController@random');

    /**
     * @api {get} /api/breweries/beers Request a Random Beer From a Brewery
     * @apiName RandomBeerByBrewery
     * @apiGroup Brewery
     *
     * @apiParam {String} brewery_id Brewery Identifier.
     */
    Route::get('/breweries/beers', 'BreweryController@beers');

    /**
     * @api {get} /api/entities/search Search for both beers and breweries
     * @apiName SearchEntity
     * @apiGroup Search
     *
     * @apiParam {String} type Entity Type, accepts: beer, brewery.
     * @apiParam {String} query Search term.
     */
    Route::get('/entities/search', 'EntityController@search');
});
