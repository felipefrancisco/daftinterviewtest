# DistilledSCH's Software Developer Technical Assessment

---

## Summary

- This project was created using **Laravel 5.3** and PHP **5.6**. The api client is optimized to prevent unecessary requests to the api by using the 
LocalStorage engine and Memcached (if available). Laravel was used just for handling the dependecy injection and the routes. All created classes 
can be found on the following folders: `./app/Http/Controllers`, `./app/Services`, `./app/Library/`. 

- Every time when the `/beers/random` endpoint is requested, the response is cached in the LocalStorage, so, in the following 10 times the user
press the "Another Beer" button, the LocalStorage will be used instead of requesting the API once again.

- Every time when the user clicks "More From This Brewery", the API response is stored on Memcached. The same logic is applied to the 
search (both for beers and breweries).

- AngularJS was used to handle data binding on the front-end. You can find the js file with the controller and services in `./public/js/app.js`.

- All created classes are following the PSR-2 standard.

---

## Installation
```
git clone https://bitbucket.org/felipefrancisco/daftinterviewtest.git interview.daft
cd interview.daft
composer install
sudo chown -R www-data .
./vendor/bin/phpunit
```

## Environment Variables

Enviroment variables (such as api key, memcached host, memcached port etc) can be found on `./.env`.

## Server Configuration

This is a Laravel based application, so the `root` entry on the server file must be
pointing to the `./public` folder of this project. Here are examples of server entries:

### Nginx
```
server {

        listen   80;
        server_name  felipe.interview.daft.app;
        
        root "/var/www/interview.daft/public/";        
        index index.php index.html index.html;
        
        ... local php/fpm config
}
```
If more info is needed, check this link:  https://www.digitalocean.com/community/tutorials/how-to-install-laravel-with-an-nginx-web-server-on-ubuntu-14-04


### Apache
```
<VirtualHost *:80>

  ServerName felipe.interview.daft.app  
  DocumentRoot "/var/www/interview.daft/public"
  
  <Directory "/var/www/interview.daft/public">
    AllowOverride All
  </Directory>
  
  ... other config
  
</VirtualHost>
```
If more info is needed, check this link: https://www.howtoforge.com/tutorial/install-laravel-on-ubuntu-for-apache/

### Hosts entry 
```
127.0.0.1   felipe.interview.daft.app
```

---

## Tests Cases

Tests can be found on `./tests`.

---
Do not hesitate to contact me :)
Felipe Francisco - felipefrancisco@outlook.com - +353 (0) 83 851 8599