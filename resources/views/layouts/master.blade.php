<!DOCTYPE html>
<html lang="en" ng-app="breweryApiTest">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BreweryDB API - Daft.ie Test</title>

        <!-- Stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css"  rel="stylesheet" type="text/css"> -->
        <link href="/bower_components/sweetalert2/dist/sweetalert2.min.css"  rel="stylesheet" type="text/css">
        <link href="/bower_components/PACE/themes/blue/pace-theme-flash.css"  rel="stylesheet" type="text/css">
        <link href="/css/app.css"  rel="stylesheet" type="text/css">

        <script src="bower_components/es6-promise/es6-promise.min.js"></script>

    </head>
    <body>
        @yield('content')
    </body>


    <!-- Scripts -->
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower_components/angular/angular.min.js"></script>
    <script src="/bower_components/basil.js/build/basil.min.js"></script>
    <script src="/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="/bower_components/PACE/pace.min.js"></script>

    <script src="/js/app.js"></script>
</html>
