@extends('layouts.master')

@section('content')

    <div class="container" ng-controller="HomeController as vm">

        <div class="row">
            <div class="col-md-12">
                <h1 class="title">
                   DistilledSCH Beer Applications
                </h1>
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img ng-src="@{{beer.picture}}" class="picture img-responsive img-square" ng-show="beer.picture" />
                            </div>
                            <div class="col-md-7">
                                <h3 class="title" ng-bind="beer.name">Loading...</h3>
                                <p ng-bind="beer.description"></p>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-group-vertical" role="group">
                                    <button class="btn btn-primary" ng-click="vm.events.getRandomBeers()">Another Beer</button>
                                    <button class="btn btn-success" ng-click="vm.events.getBeersByBrewery()">More From This Brewery</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form class="form" name="search">
                    <h2 class="title">
                        Search
                    </h2>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="query" ng-model="srch.query">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" selected="selected" name="type" class="form-group" value="beer" ng-model="srch.type">
                                                Beer
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" selected="selected" name="type" class="form-group" value="brewery" ng-model="srch.type">
                                                Brewery
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary" ng-click="vm.events.performSearch()" ng-disabled="vm.disableSearchButton()">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row ng-hide" ng-show="vm.displaySearchResults()">
            <div class="col-md-12">
                <h2 class="title">
                    Search Results
                </h2>
            </div>
        </div>

        <div class="row ng-hide" ng-show="vm.displaySearchResults()">

            <div class="col-md-12">
                <div class="panel" ng-show="!srch.results.length">
                    <div class="panel-body">
                        No results found.
                    </div>
                </div>

                <div class="panel" ng-repeat="item in srch.results | limitTo:quantity">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <span ng-show="!item.picture">
                                    No Picture.
                                </span>
                                <img ng-src="@{{item.picture}}" class="img-square img-responsive" ng-show="item.picture" />
                            </div>
                            <div class="col-md-9">
                                <h4 class="heading title" ng-bind="item.name"></h4>
                                <p ng-bind="item.description"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ng-hide" ng-show="vm.displayShowMore()">
            <div class="col-md-12">
                <div class="text-center">
                    <a ng-click="vm.showMore()" class="btn btn-block btn-primary">Show More</a>
                </div>
            </div>
        </div>

    </div>

@endsection